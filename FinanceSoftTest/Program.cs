﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace FinanceSoftTest1
{
    class Program
    {
        static void Main(string[] args)
        {
            EncodeString(); //вызов метода
            Console.ReadKey(); 
        }
        public static void EncodeString() //метод возвращает перевернутую строку
        {

            string str = "abcd";
            char[] chars = str.ToCharArray();
            for (int i = 0, j = str.Length - 1; i < j; i++, j--)
            {
                chars[i] = str[j];
                chars[j] = str[i];
            }

            Console.WriteLine(new string(chars));
            

        }
    }
}
