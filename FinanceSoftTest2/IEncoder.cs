﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinanceSoftTest2
{
    public interface IEncoder
    {
        void EncodeString(string str);
        void EncodeSha(string str);
    }
}
