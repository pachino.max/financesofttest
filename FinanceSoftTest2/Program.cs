﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace FinanceSoftTest2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите слово для хеширования:");
            string text = Console.ReadLine(); 
            Console.WriteLine("Каким методом хешировать: 1-старый метод, 2-метод SHA-1"); 
            string choice = Console.ReadLine(); 
            switch (choice) 
            { 
                case "1": 
                    IEncoder encoder = new Encoder();
                    encoder.EncodeString(text);
                    break;
                case "2": 
                    IEncoder shaEncoder = new Encoder();
                    shaEncoder.EncodeSha(text);
                    break;
                default: 
                    Console.WriteLine("Вы ввели неверные данные"); 
                    break;
                }
            Console.ReadKey();
        }
    }
}
