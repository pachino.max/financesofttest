﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace FinanceSoftTest2
{
    public class Encoder : IEncoder
    {
        public void EncodeSha(string str)
        {
            var sha1 = SHA1.Create();
            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(str));

            var sb = new StringBuilder();
            foreach (byte temp in hash)
            {
                sb.AppendFormat("{0:x2}", temp);
            }

            Console.WriteLine(sb.ToString());
        }

        public void EncodeString(string str)
        {
            string res = "";
            foreach (char ch in str)
            {
                char s = ch;
                s++;
                res += s;
            }

            Console.WriteLine(res);
        }

    }

}
